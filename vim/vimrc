" Vimrc configuration



" General "


" Vim, not vi
set nocompatible

" Stored history
set history=500

" Enable filetype plugins
filetype plugin on
filetype indent on

" Auto read a file when it is externally changed
set autoread

" Syntax highlighting
syntax enable

" Use UTF-8 as standard encoding
set encoding=utf8



" UI Layout "


" Visual settings
set background=dark

" Use WiLd menu
set wildmenu

" Always show position
set ruler

" Sane backspace
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Sane search
set hlsearch
set incsearch
set smartcase
set ignorecase

" Don't redraw while executing macros
set lazyredraw

" Show matching bracket
set showmatch

" Stop vim from causing the bell sound on errors
set noerrorbells
set novisualbell

" Numbered lines
set number

" Fold settings
set foldenable
set foldlevelstart=10
set foldnestmax=5
nnoremap <space> za
set foldmethod=indent

" Set mode
set modelines=1



" Swap settings "


" Use swap files
set swapfile

" Store swp files in ~/.tmp
set dir=~/.tmp


" Indentation "


" Set tab width to 4 spaces
set tabstop=8
set softtabstop=8

" Do NOT expand tabs to spaces
set noexpandtab

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines
