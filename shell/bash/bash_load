#!/usr/bin/env bash

function safe_source() { 
	# check if $1 exists; If it does; source it.
	# returns 1 if sourcing fails and 64 if no arguments are provided

	if [[ -n "${1}" ]]; then
		[[ -e "${1}" ]] && { . "$1" && return 0; } || return 1
	else
		return 64
	fi
}

function appendpath() {
	# Takes $1, to append to path, and pathvar, the env to append it to
	# Checks if path already has the value within it before appending
	# if $2 is not provided, PATH is used
	# NOTE: Uses eval; While some steps towards sanitization are taken
	# do _not_ pass untrusted data into this function

	[[ "${#}" -eq 2 ]] && PATHOPT="${2}" || PATHOPT='PATH'
	[[ -n "\$PATHOPT" ]] || return 1
	PATH_VALUE=$(eval printf "\$${PATHOPT}") || return 1
	if [[ ":${PATH_VALUE}:" != *":${1}:"*  ]] ; then
		eval export "${PATHOPT}=\$${PATHOPT}:${1}"
	fi
}

safe_source "${HOME}/.bash_config" 


if [[ -n "${DOTFILE_BASH_CONFIG_PATH}" ]]; then
	if [[ $- == *i*  ]]; then # Only source if session is interactive
		safe_source "${DOTFILE_BASH_CONFIG_PATH}/bash_aliases"
		safe_source "${DOTFILE_BASH_CONFIG_PATH}/bash_functions"
		safe_source "${DOTFILE_BASH_CONFIG_PATH}/bash_defaults"
	fi
	safe_source "${DOTFILE_BASH_CONFIG_PATH}/bash_session"
fi

if [[ -n "${LOCAL_BASH_CONFIG_PATH}" ]]; then
	if [[ ${-} == *i* ]]; then # Only source if session is interactive
		safe_source "${LOCAL_BASH_CONFIG_PATH}/bash_aliases"
		safe_source "${LOCAL_BASH_CONFIG_PATH}/bash_functions"
		safe_source "${LOCAL_BASH_CONFIG_PATH}/bash_defaults"
	fi
	safe_source "${LOCAL_BASH_CONFIG_PATH}/bash_session"
fi

# end bash_load
