=== Bash 'dotfiles' ===


== Description ==

This directory contains bash aliases/functions/settings/defaults/etc to allow for a more productive workflow.
The local bash configuration files should source ./bash_load, which handles deciding which files to source.
Additonally, bash_load can be safely moved/modified. Configuration options belong in bash_config.


== Install ==

- create a $HOME/.bash_config (or copy ./bash_config)
- configure $HOME/.bash_config if necessary (i.e; comment out the line that sources $HOME/.bash_config )
- source bash_load from relevant login files (usually $HOME/.bashrc and $HOME/.bash_profile or $HOME/.bash_login)


== Dependencies==

- GNU BASH


== Files ==

= bash_config =
Configuration options go here

= bash_load =
Sanity checks enviroment
Sources other bash configuration files

= bash_session =
Configuration which should be used regardless of session type

= bash_aliases =
Aliases go here

= bash_functions =
Functions which modify or interact with the enviroment/session go here

= bash_defaults =
Sets configuration defaults to be used in interactive sessions


== Bugs! ==

There are no known bugs. That doesn't mean they aren't there. I just don't know about them.


== Contact ==

Please direct bug reports/concerns/inquries to kurisu+dotfiles@openmailbox.org
